# API REST Node

API Rest escucha por defecto en el puerto 3000

## Instalar dependencias
```
npm install
```

## Ejecutar proyecto
```
node src/index.js
```

## Servicio REST expuesto
```
Servicio GET: http://localhost:3000
```

## Generar imagen docker
```
docker build -t api-node .
```
