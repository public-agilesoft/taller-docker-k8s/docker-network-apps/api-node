'use strict';

const express = require('express');
const cors = require('cors');
const axios = require('axios');
const app = express();
const port = process.env.PORT | 3000;

app.use(cors());
app.get('/', (req, res) => {
    const date = new Date();
    console.log(`Request received at ${date}`);
    res.json({
        data: `Hi! The time is ${date}`,
        id: `${process.pid}`,
    });
});

app.get('/integration', async (req, res) => {
    const response = await axios.get('http://api-interna-node:4000');
    res.json(response.data);
});

app.listen(port, () => {
  console.log(`Example app listening at port ${port}`);
});
